import 'package:flutter/material.dart';
import 'package:location/location.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'location',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Location location = new Location();

  late bool _serviceEnabled;
  late PermissionStatus _permissionGranted;
  LocationData? _locationData;

  // _HomePageState(this._serviceEnabled, this._permissionGranted);

  Future<void> _getUserLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if(!_serviceEnabled) {
      _serviceEnabled = await location.requestService();

      if(!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if(_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();

      if(_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    LocationData currentLocation = await location.getLocation();

    setState(
            () {
          _locationData = currentLocation;
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Column (
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              const Text('Aiman bin Anizan'),

              const SizedBox(
                height: 25,
              ),

              ElevatedButton(
                  onPressed: _getUserLocation,
                  child: const Text('Get Location')
              ),

              const SizedBox(
                height: 25,
              ),

              _locationData != null ?
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Wrap(
                  children: [
                    Text('Your latitude: ${_locationData!.latitude}'),

                    const SizedBox(
                      width: 10,
                    ),

                    Text('Your longtitude: ${_locationData!.longitude}')
                  ],
                ),
              )

                  :Container()
            ],
          ),
        )
    );
  }
}
